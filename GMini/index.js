global.environnement = "production" // 'production' or 'development'

if (global.environnement=="development"){
    process.env["API_KEY_SHA256"] = "98483c6eb40b6c31a448c22a66ded3b5e5e8d5119cac8327b655c8b5c4836489";
    //in development mode, secret key is "testkey"
}
const app = require('express')();
const server = require('http').createServer(app);
global.io = require('socket.io')(server);
const port = process.env.PORT || 8080;

/** 
 * Require local scripts
 */
const setconf = require(process.cwd() + '/src/index/setconf');
const middelware = require(process.cwd() + '/src/index/middelware');
const static =  require(process.cwd() + '/src/static/index');
const routing = require(process.cwd() + '/src/index/routing');
const errorhandling = require(process.cwd() + '/src/index/errorhandling');

const api = require(process.cwd() + '/src/api/index');


global.serverdata = {
    config: {
        "testparam": "test",
        "minx": 0,
        "miny": 0,
        "maxx": 1000,
        "maxy": 30,
        "online": true,
        "statusmsg": ""
    },
    data: {
        "altitude_0": {"ts": [],"values": []},
        "altitude_1": {"ts": [],"values": []},
        "altitude_2": {"ts": [],"values": []},
        "descVelocity_0": {"ts": [],"values": []},
        "descVelocity_1": {"ts": [],"values": []},
        "descVelocity_2": {"ts": [],"values": []},
        "tempThermis1_0": {"ts": [],"values": []},
        "tempThermis1_1": {"ts": [],"values": []},
        "tempThermis1_2": {"ts": [],"values": []},
        "SubCanEjected_0": {"ts": [],"values": []},
        "SubCanEjected_1": {"ts": [],"values": []},
        "SubCanEjected_2": {"ts": [],"values": []},
    }
};
global.connectedUsers = 0;

setconf(app);

app.use(middelware.AlwaysUse);

app.use(static);

app.use('/api', api);

app.use(routing);

app.use(errorhandling);


global.io.on('connection', (socket) => {
    global.connectedUsers++;
    socket.emit('welcome', global.serverdata);
    socket.on('disconnect', () => {
        global.connectedUsers--;
    })
});

server.listen(port, () => {
    console.log(`Listening on port ${port}.`);
});