document.getElementById("sendrequest").onclick = function (){
    var xhr = new XMLHttpRequest();
    var url = document.getElementById('url').innerHTML;
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            var res = xhr.responseText;
            console.log(JSON.parse(res));
            document.getElementById("response").innerHTML = res;
        }
    };
    var data = document.getElementById("request").value;
    xhr.send(data);
};