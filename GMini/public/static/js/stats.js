function getConnectedUsers() {
    var xhr = new XMLHttpRequest();
    var url = "/api/stats";
    xhr.open("GET", url, true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            var res = JSON.parse(xhr.responseText);
            document.getElementById("connectedUsers").innerHTML = res.connectedUsers;
        }
    };
    xhr.send();
    setTimeout(getConnectedUsers, 15000);
}

getConnectedUsers();