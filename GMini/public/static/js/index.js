var socket = io();

dataloaded = false;
serverdata = {};
socket.on('welcome', (existingdata) => {
    serverdata = existingdata;
    updateGraphData();
    makeGraph();
    checkStatus();
    if(!dataloaded) {
        dataloaded = true;
        socket.on('addData', (data) => {
            data.forEach(element => {
                if(!serverdata.data[element.category]["ts"].includes(element.ts)) {
                    if(!((element.category.startsWith("altitude")  && element.value == -1)
                    || (element.category.startsWith("descVelocity") && element.value == 9999.9)
                    || (element.category.startsWith("tempThermis1") && element.value == -9999.9))) {
                        serverdata.data[element.category]["ts"].push(element.ts);
                        serverdata.data[element.category]["values"].push(element.value);
                    }
                }
            });
            updateGraphData();
            updateGraph();
        });
        socket.on('modifyConfig', (param, value) => {
            window.serverdata.config[param] = value;
            updateGraphData();
            updateGraph();
            
        });
        socket.on('reset', () => {
            location.reload();
        });
    }
});



graphData = {

    alt0Trace: {
        x: [],
        y: [],
        type: 'scatter',
        mode: 'markers',
        marker: {
            color: 'rgb(0, 0, 255)',
            size: 5
        },
        connectgaps: true,
        name: "Altitude MainCan (m)"
    },

    alt1Trace: {
        x: [],
        y: [],
        type: 'scatter',
        mode: 'markers',
        marker: {
            color: 'rgb(0, 255, 0)',
            size: 5
        },
        connectgaps: true,
        name: "Altitude SubCan1 (m)"
    },

    alt2Trace: {
        x: [],
        y: [],
        type: 'scatter',
        mode: 'markers',
        marker: {
            color: 'rgb(255, 0, 0)',
            size: 5
        },
        connectgaps: true,
        name: "Altitude SubCan2 (m)"
    },
   


    data: [ ],

    layout: {
        title:'Altitude versus time',
        height: 500,
        // font: {size: 18},
        xaxis: {
            title: 'Time since power on (s)',
            showgrid: false,
            showline:false,
            zeroline:true,
            fixedrange: true
        },
        
        
        yaxis: {
            title: 'Altitude (m)',
            showline: false,
            zeroline:true,
            fixedrange: true
        }
    },

    config: {
        responsive: true,
        staticPlot: false,
        modeBarButtonsToRemove:[
            'select2d',
            'lasso2d',
            'toggleSpikelines',
            'hoverClosestCartesian',
            'hoverCompareCartesian'
        ],
        displaylogo: false,
        displayModeBar: true,
        toImageButtonOptions: {
            format: 'png',
            filename: 'GMiniAltitudeAndTime',
            height: 600,
            width: 800,
            scale: 1
        }
    }
}



graph2Data = {

    temp0Trace: {
        x: [],
        y: [],
        type: 'scatter',
        mode: 'markers',
        marker: {
            color: 'rgb(0, 0, 255)',
            size: 5
        },
        connectgaps: true,
        name: "Temperature MainCan (°C)"
    },

    
    temp1Trace: {
        x: [],
        y: [],
        type: 'scatter',
        mode: 'markers',
        marker: {
            color: 'rgb(0, 255, 0)',
            size: 5
        },
        connectgaps: true,
        name: "Temperature SubCan1 (°C)"
    },

    temp2Trace: {
        x: [],
        y: [],
        type: 'scatter',
        mode: 'markers',
        marker: {
            color: 'rgb(255, 0, 0)',
            size: 5
        },
        connectgaps: true,
        name: "Temperature SubCan2 (°C)"
    },

    data: [ ],

    layout: {
        title:'Temperature versus time',
        height: 500,
        // font: {size: 18},
        xaxis: {
            title: 'Time since power on (s)',
            showgrid: false,
            showline:false,
            zeroline:true,
            fixedrange: true
        },
        yaxis: {
            title: 'Temperature (°C)',
            showline: false,
            zeroline:true,
            fixedrange: true
        }
        
    },

    config: {
        responsive: true,
        staticPlot: false,
        modeBarButtonsToRemove:[
            'select2d',
            'lasso2d',
            'toggleSpikelines',
            'hoverClosestCartesian',
            'hoverCompareCartesian'
        ],
        displaylogo: false,
        displayModeBar: true,
        toImageButtonOptions: {
            format: 'png',
            filename: 'GMiniTelemetryTemperatureAndTime',
            height: 600,
            width: 800,
            scale: 1
        }
    }
}

graph3Data = {

    descVelocity0Trace: {
        x: [],
        y: [],
        type: 'scatter',
        mode: 'markers',
        marker: {
            color: 'rgb(0, 0, 255)',
            size: 5
        },
        connectgaps: true,
        name: "Velocity MainCan (m/s)"
    },

    descVelocity1Trace: {
        x: [],
        y: [],
        type: 'scatter',
        mode: 'markers',
        marker: {
            color: 'rgb(0, 255, 0)',
            size: 5
        },
        connectgaps: true,
        name: "Velocity SubCan1 (m/s)"
    },

    descVelocity2Trace: {
        x: [],
        y: [],
        type: 'scatter',
        mode: 'markers',
        marker: {
            color: 'rgb(255, 0, 0)',
            size: 5
        },
        connectgaps: true,
        name: "Velocity SubCan2 (m/s)"
    },
   


    data: [ ],

    layout: {
        title:'Velocity versus time',
        height: 500,
        // font: {size: 18},
        xaxis: {
            title: 'Time since power on (s)',
            showgrid: false,
            showline:false,
            zeroline:true,
            fixedrange: true
        },
        
        
        yaxis: {
            title: 'Velocity (m/s)',
            showline: false,
            zeroline:true,
            fixedrange: true
        }
    },

    config: {
        responsive: true,
        staticPlot: false,
        modeBarButtonsToRemove:[
            'select2d',
            'lasso2d',
            'toggleSpikelines',
            'hoverClosestCartesian',
            'hoverCompareCartesian'
        ],
        displaylogo: false,
        displayModeBar: true,
        toImageButtonOptions: {
            format: 'png',
            filename: 'GMiniVelocityAndTime',
            height: 600,
            width: 800,
            scale: 1
        }
    }
}

function makeGraph() {
    Plotly.newPlot('graph', graphData.data, graphData.layout, graphData.config).then(function() { document.getElementById('graphLoading').hidden = true; });
    Plotly.newPlot('graph2', graph2Data.data, graph2Data.layout, graph2Data.config).then(function() { document.getElementById('graph2Loading').hidden = true; });
    Plotly.newPlot('graph3', graph3Data.data, graph3Data.layout, graph3Data.config).then(function() { document.getElementById('graph3Loading').hidden = true; });

}

function updateGraphData() {
    graphData.layout.xaxis.range=[serverdata.config["minx"],serverdata.config["maxx"]];
    graph2Data.layout.xaxis.range=[serverdata.config["minx"],serverdata.config["maxx"]];
    graph3Data.layout.xaxis.range=[serverdata.config["minx"],serverdata.config["maxx"]];

    graphData.alt0Trace.x = serverdata.data["altitude_0"]["ts"];
    graphData.alt0Trace.y = serverdata.data["altitude_0"]["values"];

    graphData.alt1Trace.x = serverdata.data["altitude_1"]["ts"];
    graphData.alt1Trace.y = serverdata.data["altitude_1"]["values"];

    graphData.alt2Trace.x = serverdata.data["altitude_2"]["ts"];
    graphData.alt2Trace.y = serverdata.data["altitude_2"]["values"];

    graphData.data = [graphData.alt0Trace, graphData.alt1Trace, graphData.alt2Trace];


    graph2Data.temp0Trace.x = serverdata.data["tempThermis1_0"]["ts"];
    graph2Data.temp0Trace.y = serverdata.data["tempThermis1_0"]["values"];

    graph2Data.temp1Trace.x = serverdata.data["tempThermis1_1"]["ts"];
    graph2Data.temp1Trace.y = serverdata.data["tempThermis1_1"]["values"];

    graph2Data.temp2Trace.x = serverdata.data["tempThermis1_2"]["ts"];
    graph2Data.temp2Trace.y = serverdata.data["tempThermis1_2"]["values"];

    graph2Data.data = [graph2Data.temp0Trace, graph2Data.temp1Trace, graph2Data.temp2Trace];


    graph3Data.descVelocity0Trace.x = serverdata.data["descVelocity_0"]["ts"];
    graph3Data.descVelocity0Trace.y = serverdata.data["descVelocity_0"]["values"];

    graph3Data.descVelocity1Trace.x = serverdata.data["descVelocity_1"]["ts"];
    graph3Data.descVelocity1Trace.y = serverdata.data["descVelocity_1"]["values"];

    graph3Data.descVelocity2Trace.x = serverdata.data["descVelocity_2"]["ts"];
    graph3Data.descVelocity2Trace.y = serverdata.data["descVelocity_2"]["values"];

    graph3Data.data = [graph3Data.descVelocity0Trace, graph3Data.descVelocity1Trace, graph3Data.descVelocity2Trace];

    document.getElementById("statusmsg").innerHTML = serverdata.config["statusmsg"];
    if(serverdata.config["statusmsg"] === "") {
        document.getElementById("statusmsg").hidden = true;
    }

    else {
        document.getElementById("statusmsg").hidden = false;
    }
}

function updateGraph() {
    Plotly.update('graph', graphData.data, graphData.layout, graphData.config);
    Plotly.update('graph2', graph2Data.data, graph2Data.layout, graph2Data.config);
    Plotly.update('graph3', graph3Data.data, graph3Data.layout, graph3Data.config);

        
}

function checkStatus() {
    document.getElementById("establishingconnection").hidden = true;
    document.getElementById("liveupdates").hidden = !(socket.connected && serverdata.config["online"]);
    document.getElementById("currentlyoffline").hidden = (socket.connected && serverdata.config["online"]);
    setTimeout(checkStatus, 5000);

}

//if couldn't establish connection after 10 seconds, run check status anyways
setTimeout(checkStatus, 10000);