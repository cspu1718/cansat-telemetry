module.exports = {
    err400: err400,
    err401, err401,
    err403: err403,
    err404: err404,
    err500: err500
};

function err400 (req, res){
    const jsonres = {
        status: 400,
        message: "Bad Request."
    }
    res.status(400).json(jsonres);
}

function err401 (req, res){
    const jsonres = {
        status: 401,
        message: "Unauthorized."
    }
    res.status(401).json(jsonres);
}

function err403 (req, res) {
    const jsonres = {
        status: 403,
        message: "Forbidden."
    };
    res.status(403).json(jsonres);
}

function err404 (req, res) {
    const jsonres = {
        status: 404,
        message: "Not Found."
    };
    res.status(404).json(jsonres);
}

function err500 (err, req, res, next) {
    console.error(err.stack);
    const jsonres = {
        status: 500,
        message: "Internal server error."
    };
    res.status(500).json(jsonres);
}
