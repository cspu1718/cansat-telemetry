const router = require('express').Router();
const Joi = require('joi');


const middelware = require(process.cwd() + '/src/api/middelware');
const errors = require(process.cwd() + '/src/api/errors');
router
    .route('/reset')
    .post(middelware.apiauth, (req, res) => {
        global.serverdata.data={
            "altitude_0": {"ts": [],"values": []},
            "altitude_1": {"ts": [],"values": []},
            "altitude_2": {"ts": [],"values": []},
            "descVelocity_0": {"ts": [],"values": []},
            "descVelocity_1": {"ts": [],"values": []},
            "descVelocity_2": {"ts": [],"values": []},
            "tempThermis1_0": {"ts": [],"values": []},
            "tempThermis1_1": {"ts": [],"values": []},
            "tempThermis1_2": {"ts": [],"values": []},
            "SubCanEjected_0": {"ts": [],"values": []},
            "SubCanEjected_1": {"ts": [],"values": []},
            "SubCanEjected_2": {"ts": [],"values": []},
        };
        res.status(200).send("reset complete");
        global.io.emit("reset");
    })
    .get((req, res) => {
        const defaultquery = `{
    "key":"testkey"
}
        `;
        res.render('pages/apiutil', {defaultquery: defaultquery, url:'/api/reset'});
    })
;


router
    .route('/addData')

    .post(middelware.apiauth, (req, res) => {
        const schema = Joi.array().items({
            timestamp: Joi.number()
                .integer()
                .required(),
            tempThermis1: Joi.number(),
            altitude: Joi.number(),
            descVelocity: Joi.number(),
            sourceID: Joi.number()
                .required(),
            SubCanEjected: Joi.boolean()
        });

        const result = schema.validate(req.body.data);
        if(!result.error) {
            let toSend = [];
            result.value.forEach(element => {
                //console.log(element);
                for(const prop in element) {
                    if(Object.prototype.hasOwnProperty.call(element, prop) && prop!="timestamp" && prop!="sourceID") {
                        toSend.push({"category":prop+"_"+element.sourceID.toString(), "ts":element.timestamp/1000, "value":element[prop]});
                        global.serverdata.data[prop+"_"+element.sourceID.toString()]["ts"].push(element.timestamp/1000);
                        global.serverdata.data[prop+"_"+element.sourceID.toString()]["values"].push(element[prop]);
                    }
                }
            });
            global.io.emit('addData', toSend);



            res.status(200).send(toSend);
        }
        else{
            errors.err400(req, res);
        }
    })

    .get((req, res) => {
        const defaultquery = `{
    
    "key":"testkey",
    "data":[
        {
            "timestamp":100000,
            "tempThermis1":18,
            "sourceID":0
        },
        {
            "timestamp":100000,
            "altitude":500,
            "sourceID":1
        },
        {
            "timestamp":100000,
            "descVelocity":5,
            "sourceID":2
        }    
    ]
}
`;
        res.render('pages/apiutil', {defaultquery: defaultquery, url:'/api/addData'});
    })
;


router
    .route('/modifyConfig')

    .post(middelware.apiauth, (req, res) => {
        const schema = Joi.object({
            param: Joi.string()
                .required(),
            value: Joi.any()
                .required()
        });

        const result = schema.validate(req.body.data);
        if(!result.error) {
            global.serverdata.config[result.value.param] = result.value.value;

            global.io.emit('modifyConfig', result.value.param, result.value.value);



            res.status(200).send({param:result.value.param, value:result.value.value});
        }
        else{
            errors.err400(req, res);
        }
    })

    .get((req, res) => {
        const defaultquery = `{
    "key":"testkey",
    "data":{
        "param":"testparam",
        "value":"test1"
    }
}
        `;
        res.render('pages/apiutil', {defaultquery: defaultquery, url:'/api/modifyConfig'});
    })
;

router
    .route("/stats")
    .get((req, res) => {
        res.status(200).send({connectedUsers: global.connectedUsers});
    })
;

module.exports = router;