module.exports = {
    err404: err404,
    err500: err500
};


function err404 (req, res, next) {
    res.status(404).render('errors/404');
}

function err500 (err, req, res, next) {
    console.error(err.stack);
    res.status(500).render('errors/500');
}
