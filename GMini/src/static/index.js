const {Router, static} = require("express");

const router = Router();



var options = {
    dotfiles: 'allow',
    etag: true,
    index: false,
    redirect: false,
    setHeaders: function (res, path, stat) {
        res.set('Cache-Control', 'no-cache');
    }
};

router.use(static(process.cwd() + '/public', options));
  

module.exports = router;