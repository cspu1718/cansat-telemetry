const {json} = require('express');

const errors = require(process.cwd() + '/src/api/errors');

const crypto = require('crypto');


module.exports = {
    AlwaysUse: [
        json()

    ],

    apiauth: apiauth
};


function apiauth (req, res, next) {
    if(crypto.createHash('sha256').update(req.body.key).digest('hex') == process.env.API_KEY_SHA256){
        next();
    }
    else{
        errors.err403(req, res, next);
    }
}

