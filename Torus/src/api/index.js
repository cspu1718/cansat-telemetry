const router = require('express').Router();

const middelware = require(process.cwd() + '/src/api/middelware');
const routing = require(process.cwd() + '/src/api/routing.js');
const errorhandling = require(process.cwd() + '/src/api/errorhandling');

router.use(middelware.AlwaysUse);
router.use(routing);
router.use(errorhandling);

module.exports = router;