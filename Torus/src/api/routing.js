const router = require('express').Router();
const Joi = require('joi');


const middelware = require(process.cwd() + '/src/api/middelware');
const errors = require(process.cwd() + '/src/api/errors');



router
    .route('/addData')

    .post(middelware.apiauth, (req, res) => {
        const schema = Joi.array().items({
            ts: Joi.number()
                .integer()
                .required(),
            temp: Joi.number(),
            alt: Joi.number(),
            vel: Joi.number(),
            len: Joi.number()
        });

        const result = schema.validate(req.body.data);
        if(!result.error) {
            let toSend = [];
            result.value.forEach(element => {
                //console.log(element);
                for(const prop in element) {
                    if(Object.prototype.hasOwnProperty.call(element, prop) && prop!="ts") {
                        toSend.push({"category":prop, "ts":element.ts, "value":element[prop]});
                        global.serverdata.data[prop]["ts"].push(element.ts);
                        global.serverdata.data[prop]["values"].push(element[prop]);
                    }
                }
            });
            global.io.emit('addData', toSend);



            res.status(200).send(toSend);
        }
        else{
            errors.err400(req, res);
        }
    })

    .get((req, res) => {
        const defaultquery = `{
    "key":"testkey",
    "data":[
        {
            "ts":126,
            "temp":13.2
        },
        {
            "ts":131,
            "temp":13.1,
            "alt":156.2
        }
    ]
}
`;
        res.render('pages/apiutil', {defaultquery: defaultquery, url:'/api/addData'});
    })
;


router
    .route('/modifyConfig')

    .post(middelware.apiauth, (req, res) => {
        const schema = Joi.object({
            param: Joi.string()
                .required(),
            value: Joi.any()
                .required()
        });

        const result = schema.validate(req.body.data);
        if(!result.error) {
            global.serverdata.config[result.value.param] = result.value.value;

            global.io.emit('modifyConfig', result.value.param, result.value.value);



            res.status(200).send({param:result.value.param, value:result.value.value});
        }
        else{
            errors.err400(req, res);
        }
    })

    .get((req, res) => {
        const defaultquery = `{
    "key":"testkey",
    "data":{
        "param":"testparam",
        "value":"test1"
    }
}
        `;
        res.render('pages/apiutil', {defaultquery: defaultquery, url:'/api/modifyConfig'});
    })
;

router
    .route("/stats")
    .get((req, res) => {
        res.status(200).send({connectedUsers: global.connectedUsers});
    })
;

module.exports = router;