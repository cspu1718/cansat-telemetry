const helmet = require('helmet');
const compression = require('compression')();

module.exports = {
    AlwaysUse: [
        compression,
        logAll,
        helmet({
            contentSecurityPolicy: false,
            frameguard: false
        }),
        setDefaultHeaders
    ],

    test: test
};

function logAll (req, res, next){
    if(global.environnement == "development"){
        res.on('finish', () => {
            const toLog = req.method + " " + req.url + " from " + req.connection.remoteAddress + ". Response: " + res.statusCode;
            console.log(toLog);
        });
    }
    next();
}

function test (req, res, next){
    console.log("TEST MIDDELWARE");

    next();
}

function setDefaultHeaders (req, res, next) {
    res.set({
        "X-Made-By": "Torus",
    });
    next();
}

