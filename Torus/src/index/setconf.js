const conf = {
    'view engine': 'ejs',
    'etag': false,
    'json escape': true,
    'case sensitive routing': false,
    'strict routing': false,
    'x-powered-by': false,
    'env': global.environnement // 'production' or 'development'
}

module.exports = function(app) {
    for(const [key, val] of Object.entries(conf)) {
        app.set(key, val);
    }
}