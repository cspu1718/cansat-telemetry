const router = require('express').Router();

const errors = require(process.cwd() + '/src/index/errors');

router.use(errors.err404);
  
router.use(errors.err500);

module.exports = router;