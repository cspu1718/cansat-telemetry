const router = require('express').Router();

router
    .route('/')
    .get((req, res) => {
        res.render('pages/index');
    })
;

router
    .route('/stats')
    .get((req, res) => {
        res.render('pages/stats', {connectedUsers: global.connectedUsers});
    })
;

router
    .route('/favicon.ico')
    .get((req, res) => {
        res.redirect('http://www.cspu.be/~uza_cansat/images/logo.gif');
    })

module.exports = router;