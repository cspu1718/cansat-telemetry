global.environnement = "production" // 'production' or 'development'

const app = require('express')();
const server = require('http').createServer(app);
global.io = require('socket.io')(server);
const port = process.env.PORT || 8080;

/** 
 * Require local scripts
 */
const setconf = require(process.cwd() + '/src/index/setconf');
const middelware = require(process.cwd() + '/src/index/middelware');
const static =  require(process.cwd() + '/src/static/index');
const routing = require(process.cwd() + '/src/index/routing');
const errorhandling = require(process.cwd() + '/src/index/errorhandling');

const api = require(process.cwd() + '/src/api/index');


global.serverdata = {
    config: {
        "testparam": "test",
        "minx": 0,
        "miny": 0,
        "maxx": 1000,
        "maxy": 30,
        "online": true
    },
    data: {
        "temp": {
            "ts": [],
            "values": []
        },
        "alt": {
            "ts": [],
            "values": []
        },
        "vel": {
            "ts": [],
            "values": []
        },
        "len": {
            "ts": [],
            "values": []
        }
    }
};
global.connectedUsers = 0;

setconf(app);

app.use(middelware.AlwaysUse);

app.use(static);

app.use('/api', api);

app.use(routing);

app.use(errorhandling);


global.io.on('connection', (socket) => {
    global.connectedUsers++;
    socket.emit('welcome', global.serverdata);
    socket.on('disconnect', () => {
        global.connectedUsers--;
    })
});

server.listen(port, () => {
    console.log(`Listening on port ${port}.`);
});