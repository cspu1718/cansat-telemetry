var socket = io();

dataloaded = false;
serverdata = {};
socket.on('welcome', (existingdata) => {
    serverdata = existingdata;
    updateGraphData();
    makeGraph();
    checkStatus();
    if(!dataloaded) {
        dataloaded = true;
        socket.on('addData', (data) => {
            data.forEach(element => {
                if(!serverdata.data[element.category]["ts"].includes(element.ts)) {
                    if(!((element.category == "alt" && element.value == -1)
                    || (element.category == "vel" && element.value == 9999.9)
                    || (element.category == "temp" && element.value == -9999.9))) {
                        serverdata.data[element.category]["ts"].push(element.ts);
                        serverdata.data[element.category]["values"].push(element.value);
                    }
                }
            });
            updateGraphData();
            updateGraph();
        });
        socket.on('modifyConfig', (param, value) => {
            window.serverdata.config[param] = value;
            updateGraphData();
            updateGraph();
        });
    }
});


graphData = {

    tempTrace: {
        x: [],
        y: [],
        type: 'scatter',
        mode: 'markers',
        marker: {
            color: 'rgb(255, 0, 0)',
            size: 10
        },
        connectgaps: true,
        name: "Temperature (°C)"
    },

    altTrace: {
        x: [],
        y: [],
        type: 'scatter',
        mode: 'markers',
        marker: {
            color: 'rgb(0, 0, 0)',
            size: 10
        },
        connectgaps: true,
        yaxis: 'y2',
        name: "Altitude (m)"
    },



    data: [ ],

    layout: {
        title:'Altitude and temperature',
        height: 500,
        // font: {size: 18},
        xaxis: {
            title: 'Time since power on (ms)',
            showgrid: false,
            showline:false,
            zeroline:true,
            fixedrange: true
        },
        yaxis: {
            title: 'Temperature (°C)',
            showline: false,
            zeroline:true,
            fixedrange: true
        },
        yaxis2: {
            title: 'Altitude (m)',
            showline: false,
            zeroline:true,
            fixedrange: true,
            side: 'right',
            overlaying: 'y'
        }
    },

    config: {
        responsive: true,
        staticPlot: false,
        modeBarButtonsToRemove:[
            'select2d',
            'lasso2d',
            'toggleSpikelines',
            'hoverClosestCartesian',
            'hoverCompareCartesian'
        ],
        displaylogo: false,
        displayModeBar: true,
        toImageButtonOptions: {
            format: 'png',
            filename: 'TorusTelemetryAltitudeAndTemperature',
            height: 600,
            width: 800,
            scale: 1
        }
    }
}

graph2Data = {

    velTrace: {
        x: [],
        y: [],
        type: 'scatter',
        mode: 'markers',
        marker: {
            color: 'rgb(0, 0, 255)',
            size: 10
        },
        connectgaps: true,
        name: "Velocity (m/s)"
    },

    lenTrace: {
        x: [],
        y: [],
        type: 'scatter',
        mode: 'markers',
        marker: {
            color: 'rgb(0, 255, 0)',
            size: 10
        },
        connectgaps: true,
        yaxis: 'y2',
        name: "Rope length (mm)"
    },



    data: [ ],

    layout: {
        title:'Velocity and rope length',
        height: 500,
        // font: {size: 18},
        xaxis: {
            title: 'Time since power on (ms)',
            showgrid: false,
            showline:false,
            zeroline:true,
            fixedrange: true
        },
        yaxis: {
            title: 'Velocity (m/s)',
            showline: false,
            zeroline:true,
            fixedrange: true
        },
        yaxis2: {
            title: 'Rope length (mm)',
            showline: false,
            zeroline:true,
            fixedrange: true,
            side: 'right',
            overlaying: 'y'
        }
    },

    config: {
        responsive: true,
        staticPlot: false,
        modeBarButtonsToRemove:[
            'select2d',
            'lasso2d',
            'toggleSpikelines',
            'hoverClosestCartesian',
            'hoverCompareCartesian'
        ],
        displaylogo: false,
        displayModeBar: true,
        toImageButtonOptions: {
            format: 'png',
            filename: 'TorusTelemetryVelocityAndLength',
            height: 600,
            width: 800,
            scale: 1
        }
    }
}

function makeGraph() {
    Plotly.newPlot('graph', graphData.data, graphData.layout, graphData.config).then(function() { document.getElementById('graphLoading').hidden = true; });
    Plotly.newPlot('graph2', graph2Data.data, graph2Data.layout, graph2Data.config).then(function() { document.getElementById('graph2Loading').hidden = true; });
}

function updateGraphData() {
    addToX = [serverdata.config["maxx"], serverdata.config["minx"], null, null];
    addToY = [null, null, serverdata.config["maxy"], serverdata.config["miny"]];

    graphData.tempTrace.x = serverdata.data["temp"]["ts"].concat(addToX);
    graphData.tempTrace.y = serverdata.data["temp"]["values"].concat(addToY);

    graphData.altTrace.x = serverdata.data["alt"]["ts"].concat(addToX);
    graphData.altTrace.y = serverdata.data["alt"]["values"].concat(addToY);

    graphData.data = [graphData.tempTrace, graphData.altTrace];

    graph2Data.velTrace.x = serverdata.data["vel"]["ts"].concat(addToX);
    graph2Data.velTrace.y = serverdata.data["vel"]["values"].concat(addToY);

    graph2Data.lenTrace.x = serverdata.data["len"]["ts"].concat(addToX);
    graph2Data.lenTrace.y = serverdata.data["len"]["values"].concat(addToY);

    graph2Data.data = [graph2Data.velTrace, graph2Data.lenTrace];
}

function updateGraph() {
    Plotly.update('graph', graphData.data, graphData.layout, graphData.config);
    Plotly.update('graph2', graph2Data.data, graph2Data.layout, graph2Data.config);    
}

function checkStatus() {
    document.getElementById("establishingconnection").hidden = true;
    document.getElementById("liveupdates").hidden = !(socket.connected && serverdata.config["online"]);
    document.getElementById("currentlyoffline").hidden = (socket.connected && serverdata.config["online"]);
    setTimeout(checkStatus, 5000);
}

//if couldn't establish connection after 10 seconds, run check status anyways
setTimeout(checkStatus, 10000);